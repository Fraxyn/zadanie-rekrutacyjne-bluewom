import React from "react";
import Products from "./components/Products";
import store from "./store";
import { Provider } from "react-redux";
import Cart from './components/Cart';

class App extends React.Component {


  render() {
    return (
      <Provider store={store}>
        <div className="grid-container" >
          <header>
            <a href="/">Strona Główna</a>
          </header>
          <main>
            <div className="content">
              <div className="main">
                <Products />
              </div>
              <div className="sidebar">
                <Cart />
              </div>
            </div>
          </main>
          <footer>
            Wiecej Informacji..
      </footer>
        </div>
      </Provider>
    );
  }
}


export default App;
