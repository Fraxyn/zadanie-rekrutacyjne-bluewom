import React, { Component } from 'react'
import { connect } from "react-redux";
import { fetchProducts } from "../actions/productActions";
import { addToCart } from "../actions/cartActions"

class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: null,
        };
    }


    componentDidMount() {
        this.props.fetchProducts();
    }


    render() {

        return (
            <div>

                {!this.props.products ? (
                    <div>Loading...</div>
                ) : (
                    <ul className="products">
                        {this.props.products.map((product) => (
                            product.rates.map((x) => (

                                <li li key={x.code} >
                                    <div className="product">
                                        <div className="description">
                                            <p className="product__header">waluta</p>
                                            <p className="product__currency">{x.currency}</p>
                                            <p className="product__code">{x.code}</p>
                                        </div>
                                        <div className="product-price">
                                            <div className="price">Kurs: {x.mid} PLN</div>
                                            <button onClick={() => this.props.addToCart(x)} className="button primary">
                                                Dodaj do ulubionych
                                        </button>
                                        </div>
                                    </div>
                                </li>

                            ))))}
                    </ul>
                )
                }

            </div>
        );
    }
}

export default connect((state) => ({ products: state.products.items }), {
    fetchProducts,
    addToCart,
})(Products);
