import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';
import { removeFromCart } from '../actions/cartActions';
import { connect } from 'react-redux';




class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showCheckout: false,
        };
    }

    render() {
        const { cartItems } = this.props;

        return (
            <div>
                {cartItems.length === 0 ? (
                    <div className="cart cart-header">Lista ulubionych - Pusta</div>
                ) : (
                    <div className="cart cart-header">
                        <p>Posiadasz ulubione</p>
                        <button className="button" onClick={() => {
                            const confirmBox = window.confirm(
                                "Na pewno chcesz to wszystko usunąć z listy ulubionych?"
                            )
                            if (confirmBox === true) {
                                cartItems.forEach(y => {
                                    this.props.removeFromCart(y)
                                });

                            }
                        }}
                        >Usuń wszystko
                       </button>
                    </div>

                )}

                <div className="cart">
                    <Fade left cascade>
                        <ul className="cart-items">

                            {this.props.cartItems.map((item) => (
                                <li key={item.code}>
                                    <div>
                                        <div className="cart-items_description">
                                            <p className="cart-items_header">waluta</p>
                                            <p className="cart-items_currency">{item.currency}</p>
                                            <p className="cart-items_code">{item.code}</p>
                                            <p className="cart-items_mid">kurs: <span>{item.mid}</span> PLN</p>
                                        </div>
                                        <div className="right">
                                            <button className="button" onClick={() => {
                                                const confirmBox = window.confirm(
                                                    "Na pewno chcesz to usunąć z listy ulubionych?"
                                                )
                                                if (confirmBox === true) {
                                                    this.props.removeFromCart(item)
                                                }
                                            }}
                                            >
                                                Usuń z ulubionych
                                                 </button>

                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </Fade>
                </div>

            </div >
        );
    }
}

export default connect(
    (state) => ({
        cartItems: state.cart.cartItems,
    }),
    {
        removeFromCart,

    }
)(Cart);
