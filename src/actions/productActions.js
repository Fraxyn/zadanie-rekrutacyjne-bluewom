import { FETCH_PRODUCTS } from '../types';


export const fetchProducts = () => async (dispatch) => {
    const res = await fetch('http://api.nbp.pl/api/exchangerates/tables/a/?format=json');
    const results = await res.json();
    dispatch({
        type: FETCH_PRODUCTS,
        payload: results,
    });
};
